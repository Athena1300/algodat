package at.dgi.algo.generator;

import at.dgi.algo.bubble.Bubble;
import at.dgi.algo.insertion.Insertion;
import at.dgi.algo.selection.Selection;

public class DataTest {
	static int[] arr= DataGenerator.generateDataArray(17,20,60);
	
	public static void main(String[] args) {
		Bubble.sort(arr);
		DataGenerator.printArray(arr);
	}
}
