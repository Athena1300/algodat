package at.dgi.algo.generator;

import java.util.Arrays;
import java.util.Random;

public class DataGenerator {

	private static Random random = new Random();
	
	public static int[] generateDataArray(int size) {
		int[] arr = new int[size];
		for(int i=0;i<size;i++) {
			arr[i] = random.nextInt();
		}
		return arr;
	}
	
	public static int[] generateDataArray(int size, int min, int max) {
		int[] arr = new int[size];
		for (int i=0;i<size;i++) {
			arr[i] = random.nextInt(max-min)+min;
		}
		return arr;
	}

	static void printArray(int[] arr) {
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
}
