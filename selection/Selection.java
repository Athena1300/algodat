package at.dgi.algo.selection;

public class Selection {

	public static void sort(int[]arr) {
		int n = arr.length;
		for(int i=0;i<n-1;i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
}
