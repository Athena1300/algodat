package at.dgi.algo.radix;

import java.io.*; 
import java.util.*;

import at.dgi.algo.generator.DataGenerator; 

public class Radix {

	public static int getMax(int[]arr){
		int max = arr[0];
		for(int i=1;i<arr.length;i++) {
			if(arr[i]>max) {
				max = arr[i];
			}
		}
		return max;
	}
	
	public static int getDigit(int max) {
		int digit = String.valueOf(max).length();
		return digit;
	}
	
	public static void sort(int[]arr) {
		int max = getMax(arr);
		int digit = getDigit(max);
		for(int i= 0; i<digit; i++) {
			int [] [] sortingArray = new int [arr.length] [2];
			for(int j=0; j<arr.length;j++) {
				sortingArray [j] [0]= j; 			
				sortingArray [j] [1] = (int) ((arr[j]/Math.pow(10,i)) %10);
			}
			
			Insertion.sort(sortingArray);
			
			int [] outputArray = new int [arr.length];
			for(int j=0; j<arr.length;j++) {
				outputArray[j] = arr[sortingArray[j][0]]; 
			}
			
			
			for(int j=0; j<arr.length;j++) {
				arr[j] =  outputArray[j];
			}
		}
	}
}
