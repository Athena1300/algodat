package at.dgi.algo.radix;

public class Insertion {

	public static void sort(int[][]arr) {
		int n = arr.length;
		for(int i=0;i<n;i++) {
			int key0 = arr[i][0];
			int key1 = arr[i][1];
			int j = i-1; //-1
			
			while(j>=0 && arr[j][1]>key1) {
				arr[j+1][1]=arr[j][1];
				arr[j+1][0]=arr[j][0];
				j=j-1;
			}
			arr[j+1][0]=key0;
			arr[j+1][1]=key1;
		}
	}
}
